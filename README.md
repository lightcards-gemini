# Lightcards Gemini capsule

gemini://armaanb.net/lightcards

Gemini capsule for [lightcards](https://git.sr.ht/~armaan/lightcards)

## License
Copyright 2021 Armaan Bhojwani, MIT license.
